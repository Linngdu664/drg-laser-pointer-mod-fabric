package com.linngdu664.drg_laser_pointer;

import net.fabricmc.api.ModInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main implements ModInitializer {
    public static final Logger LOGGER = LoggerFactory.getLogger("drg_laser_pointer");
    @Override
    public void onInitialize() {
        LOGGER.info("ROCK! AND! STONE!");
        Register.register();
    }
}
