package com.linngdu664.drg_laser_pointer;

import com.linngdu664.drg_laser_pointer.items.LaserPointer;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class Register {
    public static final Item LASER_POINTER = new LaserPointer(new FabricItemSettings().maxCount(1).group(ItemGroup.MISC));
    public static void register(){
        Registry.register(Registry.ITEM,new Identifier("drg_laser_pointer","laser_pointer"),LASER_POINTER);
    }
}
